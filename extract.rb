# script pour extraire les fonctions des fichiers sources
# de linux pour les exercices de mon jeu du projet2d de inf5071

# Contient les specificités ( commentaires, mots clés, ... ) 

$defLetter = 1
$defNumber = 2
$defSpecial = 10

class Language
  attr_accessor :name, :mlCommentIn, :mlCommentOut, :singleComment

  def initialize
    @name = "C - Unix"
    @mlCommentIn = "/*"
    @mlCommentOut = "*/" 
    @singleComment = "//" 
  end

  def setToLanguage(name, mlCommentIn, mlCommentOut, singleComment)
    @name = name
    @mlCommentIn = mlCommentIn
    @mlCommentOut = mlCommentOut 
    @singleComment = singleComment
  end
end

# une ligne de code
# @name Le nom du defi
# @stringContent Le défi en question, une ligne de code
class Challenge
  attr_accessor :name, :stringContent, :value, :level
  def initialize(  stringContent )
    @name = stringContent[0..5]
    @stringContent = stringContent
    evaluate
  end

  def to_s
    @stringContent
  end

  def size
    @stringContent.size
  end

  def evaluate 
    @level = calculateLevel
    @value = @stringContent.size * @level 
  end

  def calculateLevel
    levels = [0, 0, 0,0]
    @stringContent.chars.each do |c|
      levels[0] += 1 if c =~ /\p{Alpha}/ #[\w]/
      levels[1] += 1 if c =~/\p{Digit}/ 
      levels[2] += 1 if c =~/\p{Punct}/
      levels[3] += 1 if c =~ /[\[\]\{\}\#]/
      levels[0] += 1 if c =~/\p{Upper}/
    end
    total = levels.reduce(:+).to_f
    #    puts "#{levels[0]} == #{levels[1]} == #{levels[2]} == #{levels[3]}"
    #    return $defLetter * levels[0] / total + $defNumber*levels[1] / total \
    #          + $defSpecial * levels[2] / total + $defSpecial *levels[3] / total
    return ($defLetter * levels[0] + $defNumber*levels[1] \
            + $defSpecial * levels[2] + $defSpecial * levels[3]) / total
  end
end

class Extractor


  def initialize(path = "./source_code/string")
    @totalCount = 0; 
    @validLineCount = 0; 
    @language = Language.new 
    @challenges = Array.new
    @path = path
    extractDirectory
  end

  def cleaner(line)
    line.chomp.strip
  end

  def extractFile(file)
    openComment = false
    tempLine = ""

    File.open(file, "r") do |f|
      f.each_line do |line|
        tempLine = cleaner line
        next if tempLine.size < 12
        next if tempLine =~ /.*[,+]$/
        @challenges << Challenge.new(tempLine)
        #     puts @challenges[-1].to_s
        @totalCount += 1 
        openComment = true if tempLine =~ /\/\*.*/#? @language.mlCommentIn 
        if !openComment && tempLine =~ /\/\/.*/ 
          @challenges << Challenge.new(tempLine) 
        end
        #        @validLineCount += 1
        openComment = false if tempLine =~/.*\*\// #@language.mlCommentOut
      end
    end
  end


  def extractDirectory 
    Dir.foreach(@path) do |item|
      next if item == '.' || item == '..'
      file = File.join(File.dirname(__FILE__), @path, item)
      #      puts "====== #{file} ::"
      extractFile file if file =~ /.*\.c/
    end
  end

  def print
    select_super_easy.map(&:to_s)
  end



  def select_difficulty
    @challenges.select { |x| x.value > 150 }
  end

  def select_medium
    @challenges.select { |x| x.value > 100 && x.value < 150 }
  end

  def select_easy
    @challenges.select { |x| x.value > 75 && x.value <= 100 }
  end

  def select_super_easy
    @challenges.select { |x| x.value <= 75 }
  end
end



require 'gosu'
require 'singleton'
#require_relative './play_state'

class MenuState < GameState
  include Singleton
  attr_accessor :play_state
  FRAME_DELAY = 125.00 

  def initialize
    @current_frame = 0; 
  end

  def enter
     music.play( true )
     music.volume = 0.5
  end

  def leave
    music.volume = 0
    music.stop
  end

  def music
    @@music ||= Gosu::Song.new(
      $window, Utils.musics('menu.wav'))
  end

  def update
    now = Gosu.milliseconds
    delta = now - ( @last_frame ||= now)
    if delta > FRAME_DELAY
      @last_frame = now
    end
    @current_frame += (delta / FRAME_DELAY).floor
    if @current_frame > animation.size
      @current_frame = 0
    end
  end


  def draw
    image = current_frame
    image.draw(
      0,0,20)
  end
  
  def button_down(id)
    # keyboard f1
    $window.close if id == Gosu::KbQ
    if id == Gosu::KbS
      GameState.switch(PlayState.new)
    end
  end

  def current_frame
    animation[@current_frame % animation.size]
  end

  def animation
    @@animation ||=
      Gosu::Image.load_tiles(
        $window, Utils.tiles('smallmenu.png'),
          1200, 800, false)
  end

end

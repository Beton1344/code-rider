require 'gosu'

class GameoverState < GameState
  
  def initialize
    font = Gosu.default_font_name
    @gameover = Gosu::Image.from_text(
      $window, "Game Over",
      font, 100)
    @info = Gosu::Image.from_text(
      $window, "Code Rider", 
      font, 100) 
    $window.caption = "<<< Cooooode riiiiiidddeeeeerr! >>>"
  end

  def enter
    music.play(true)
    music.volume = 0.5
  end

  def update
  end

  def draw
    @gameover.draw(
      $window.width / 2 - @gameover.width / 2, 
      $window.height / 2 - @gameover.height / 2 + 50, 
      10)
    @info.draw(
      $window.width / 2 - @info.width / 2, 
      $window.height / 2 - @info.height / 2 + 200, 
      10)
  end

  def leave
    music.stop
  end

  private
  def music 
    @@music ||= Gosu::Song.new(
      $window, Utils.musics('game_over.wav'))
  end

end

    

require 'gosu'
#require 'singleton'

require_relative '../entities/map'
require_relative '../entities/camera'
require_relative '../entities/protagoniste'
require_relative '../entities/object_pool'
require_relative '../entities/electron'
require_relative '../entities/text_control'
require_relative '../entities/launcher'
require_relative '../entities/tower'
require_relative '../entities/chip'
require_relative '../entities/bad_electron'
require_relative '../entities/tower_manager'
# require_relative '../<sources>/extract'
# require_relative '...

class PlayState < GameState
  attr_accessor :update_interval
  #include Singleton

  def initialize
    @map = Map.new
    @object_pool = ObjectPool.new(@map)
    @protagoniste = Protagoniste.new(@object_pool, *Utils.coordinate_to_xy( 50, 50 ))
    @camera = Camera.new(@protagoniste.x, @protagoniste.y)
    @camera.target = @protagoniste
    @text_control = TextControl.new(@object_pool, @camera, @protagoniste )
    @bad_electron = BadElectron.new(@object_pool, 52, 52)
    @bad_electron.target = @protagoniste
    @tower_manager = TowerManager.new(@object_pool, @protagoniste)
    @text_control.tower_manager = @tower_manager
  end

  def enter
    music.play(true)
    music.volume = 0.5
    puts "Entering the PlayState"
  end

  def leave
    music.volume = 0
    music.stop
    puts "leaving the playstate"
  end

  def update
    update_caption
    @camera.update
    @object_pool.objects.map(&:update)
    @object_pool.objects.reject!(&:removable?)
    check_for_chip
    @bad_electron.update
    quit if @protagoniste.energy < 0
  end

  def check_for_chip
    if @protagoniste.physics.aligned?
      uv = [@protagoniste.physics.u, @protagoniste.physics.v] 
    end

    unless uv == nil
      if @map.chips_values.include?(uv)
        @map.chips_values.delete(uv)
        @map.chips_setted << uv
        Chip.new(@object_pool,uv[0], uv[1])
      end
    end
  end
  
  def draw
    cam_x = @camera.x
    cam_y = @camera.y
    off_x = $window.width / 2 - cam_x
    off_y = $window.height / 2 - cam_y
    $window.translate(off_x, off_y) do
      zoom = @camera.zoom
      $window.scale(zoom, zoom, cam_x, cam_y) do
        @map.draw(@camera.viewport)
        @object_pool.objects.map { |o| o.draw(@camera.viewport)}
      end
    end
  end

  def button_down(id)
    @protagoniste.button_down(id)
    @text_control.button_down(id)
  end

  private
  def quit
    GameState.switch(GameoverState.new)
  end

  def update_caption
    now = Gosu.milliseconds
    if now - (@caption_updated_at || 0) > 1000
      $window.caption = '<<< code riders;' <<
        "[FPS: #{Gosu.fps},]  " << 
        "Prota:[ #{@protagoniste.physics.u}, #{@protagoniste.physics.v} ] | px(#{@protagoniste.physics.x}, #{@protagoniste.physics.y})" << 
        "HP : #{@protagoniste.energy}  chips : #{@map.chips_setted.size } >>> " <<
        "bad_elec [#{@bad_electron.physics.u}, #{@bad_electron.physics.v}] " << 
        "moving_in...#{ @bad_electron.moving_in}"
        @caption_updated_at = now
    end
  end

  def music
    @@music ||= Gosu::Song.new(
      $window, Utils.musics('game.wav'))
  end

end
  



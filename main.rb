require 'gosu'
require_relative 'game_window.rb'
require_relative './states/game_states'
require_relative './states/menu_state'
require_relative './states/play_state'
require_relative './states/gameover_state'

module Utils

  def self.bad_electron_speed
    10 
  end

  def self.bad_electron_interval
    35
  end

  def self.electron_speed
    20
  end
  def self.tolerance_spot
    3
  end
  def self.speed_factor
    33.33
  end
  def self.camera_zoom
    1.0 
  end
  def self.tile_size
    128
  end
  def self.map_height 
    100
  end

  def MAP_WIDTH 
    100
  end

  def self.ressource(file)
    File.join(File.dirname(__FILE__), '../assets', file)
  end

  def self.musics(file)
    File.join(File.dirname(__FILE__), '/music/', file)
  end

  def self.sounds(file)
    File.join(File.dirname(__FILE__), '/sounds', file)
  end

  def self.tiles(file)
    File.join(File.dirname(__FILE__), '/sprites', file)
  end

  def self.personnage(file)
    File.join(File.dirname(__FILE__), '../sprites', file)
  end

  def self.fonts(file)
    File.join(File.dirname(__FILE__), '../fonts', file)
  end

  def self.track_update_interval
    now = Gosu.milliseconds
    @update_interval = (now - (@last_update ||= 0)).to_f
    @last_putdate = now
  end

  def self.update_interval 
    @update_interval ||= $window.update_interval
  end

  def self.adjust_speed(speed)
    speed * update_interval / speed_factor 
  end

  def self.coordinate_to_xy(coord_x, coord_y)
    [coord_x * 128, coord_y * 128]
  end

  def self.xy_to_coordinate(x, y)
    [(x / 128).floor, (y / 128).floor ]
  end

  def self.coding_font
    fonts('UbuntuMono-R.ttf')
  end

  def self.button_down?(button)
    @buttons ||= {}
  end

  
end

$window = GameWindow.new
# puts $window.width
# puts $window.height
# play_state = PlayState.new
GameState.switch(MenuState.instance)
#GameState.switch(PlayState.new)
#GameState.switch(GameoverState.new)
$window.show

require 'gosu'
require_relative 'protagoniste'
#require_relative '../main.rb'

class Camera
  attr_accessor :x, :y, :zoom

  def initialize(x, y)
    @x = x 
    @y = y
    @zoom = Utils.camera_zoom 
  end

  def target=(target)
    @target = target
    @x, @y = target.x, target.y
    @zoom = Utils.camera_zoom 
  end

  def update
    @x, @y = @target.x + (64/@zoom), @target.y + (64/@zoom)
  end

  def to_s
    "CODE RIDER ::: FPS #{Gosu.fps}. " << 
    "#{@x}:#{@y} @ #{'%.2f' % @zoom}. " << 
    "Good luck!"
  end

  def target_delta_on_screen
    [(@x = @target.x) * @zoom, (@y - @target.y) * @zoom]
  end

  def viewport
    x0 = @x - ($window.width / 2) / @zoom #- (64 * @zoom)
    x1 = @x + ($window.width / 2) / @zoom #- (64 * @zoom)
    y0 = @y - ($window.height / 2) / @zoom #- (64 * @zoom)
    y1 = @y + ($window.height / 2) / @zoom #- (64 * @zoom)
    [x0, x1, y0, y1]
  end
end

require 'gosu'

require_relative './game_object'
require_relative './components/bad_electron_graphics'
require_relative './components/bad_electron_physics'
require_relative './components/bad_electron_sounds'

class BadElectron < GameObject

  attr_accessor :x, :y, :u, :v, :protagoniste, :sound, :physics, :moving_in

  def initialize(object_pool, u, v)
    super(object_pool)
    @u, @v = u, v
    @x, @y = Utils.coordinate_to_xy(u, v)
    @physics = BadElectronPhysics.new(self, object_pool)
    @graphics = BadElectronGraphics.new(self)
    @sound = BadElectronSounds.new(self)
    @moving_time = Utils.bad_electron_interval * 300 
    @last_move = Gosu.milliseconds  #pour donner un peu d'avance
    @damage = 10
    @bumped = [[51,50], [51,51],[52,51]]
  end

  def target=(protagoniste)
    @protagoniste = protagoniste
  end

  def moving_in
    @moving_time - (Gosu.milliseconds - @last_move)
  end

  def update
    @physics.update
    @graphics.update
    unless @protagoniste.nil?
      if moving_in < 0
        if @bumped.empty?
          next_move = @protagoniste.get_next_move
        else
          next_move = @bumped.pop
        end
        unless next_move.nil?
          @physics.move_to(next_move[0], next_move[1]) 
          @last_move = Gosu.milliseconds 
        end
      end

      if @physics.u == @protagoniste.physics.u && 
                      @physics.v == @protagoniste.physics.v && 
                      @protagoniste.physics.aligned?
          @protagoniste.energy_down(Utils.adjust_speed(@damage))
#          bump if @physics.aligned?
          @last_move = Gosu.milliseconds + (@moving_time * 2)
             

#         @protagoniste.bumped
      end

    end
  end
  def bump 
    @bumped << [@physics.u, @physics.v] 
    coord = [@physics.u + 1, @physics.v + 1]
    @physics.move_to(coord[0], coord[1])
  end
end





require 'gosu'
class ElectronPhysics < Component

    START_DIST = 2
  def initialize(game_object)
    super
    object.x, object.y = point_at_distance(START_DIST)
  end

  def trajectory_lenght
    d_x = object.target_x - x
    d_y = object.target_y - y
    Math.sqrt(d_x * d_x + d_y * d_y)
  end

  def update
    travel_speed = Utils.adjust_speed(object.speed)
    travel_distance = (Gosu.milliseconds - object.fired_at) * 0.001 * travel_speed
    object.x, object.y = point_at_distance(travel_distance)
    object.explode if arrived?
  end

  def point_at_distance(distance)
    if distance > trajectory_lenght
      return [object.target_x, object.target_y]
    end
    distance_factor = distance.to_f / trajectory_lenght
    p_x = x + (object.target_x - x) * distance_factor
    p_y = y + (object.target_y - y) * distance_factor
    [p_x, p_y]
  end

  def arrived?
    x == object.target_x && y == object.target_y
  end
end

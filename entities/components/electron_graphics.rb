class ElectronGraphics < Component
  FRAME_DELAY = 16.66 
 
  def initialize(game_object)
    super(game_object)
    @current_frame = 0
  end

  def draw(viewport)
    image = current_frame
    image.draw(
      x , 
      y , 
      20)
  end

  def update
    now = Gosu.milliseconds
    delta = now - (@last_frame ||= now) #unless @protagoniste.nil? 

    if delta > FRAME_DELAY
      @last_frame = now
    end
    @current_frame += (delta / FRAME_DELAY).floor
  end

  private

  def current_frame
    animation[@current_frame % animation.size ]
  end

  def animation
    @@animation ||= Gosu::Image.load_tiles(
      $window, Utils.tiles('electron3.png'),
      128, 128, false)
  end
end

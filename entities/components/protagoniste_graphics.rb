require 'gosu' 
require_relative 'component'
class ProtagonisteGraphics < Component
  FRAME_DELAY = 16.66
  def initialize(object_pool)
    super
    @current_frame = 0
  end

  def draw(window)
    image = current_frame
    image.draw(
      x , 
      y , 
        100)
  end

  def update
    now = Gosu.milliseconds
    delta = now - (@last_frame ||= now)
    if delta > FRAME_DELAY
      @last_frame = now 
    end
    @current_frame += (delta / FRAME_DELAY).floor
    #object.mark_for_removal if done?
  end

  private
  def current_frame
    animation[@current_frame % animation.size]
  end

  def animation
    @@animation ||=
      Gosu::Image.load_tiles(
        $window, Utils.tiles('protagoniste.png'), 
        128, 128, false)
  end
end


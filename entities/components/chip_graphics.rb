require 'gosu'
require_relative 'component'

class ChipGraphics < Component
  FRAME_DELAY = 16.66
  attr_accessor :burn
  def initialize(object_pool)
    super
    @current_frame = 0
  # @burn = false
  end

  def draw(window)
    image = current_frame
    image.draw(
      x, 
      y, 
      10)
  end

  def update
    now = Gosu.milliseconds
    delta = now - (@last_frame ||= now)
    if delta > FRAME_DELAY
      @last_frame
    end
    @current_frame += (delta / FRAME_DELAY).floor
  end

  private
  def current_frame
#    if burn
#      return animation_burn[@current_frame % animation_burn.size]
#    else
#      return animation[@current_frame % animation.size]
#    end

     animation_burn[@current_frame % animation.size]
  end

  def animation
    @@animation ||=
      Gosu::Image.load_tiles(
        $window, Utils.tiles('chip1.png'),
        128, 128, false)
  end

  def animation_burn
    @@animation_burn ||=
      Gosu::Image.load_tiles(
        $window, Utils.tiles('chip3.png'), 
        128, 128, false)
  end
end

require 'gosu'
require_relative './component'
class ProtagonisteSounds < Component
  def initialize(game_object)
    @game_object = game_object
  end
  def play
    sound.play
  end

  def sound_move
    sound.play(1, 1.5, false)
  end
  private
  def sound
    @@sound ||= Gosu::Sample.new(
      $window, Utils.sounds('quickburn2.wav'))
  end
end


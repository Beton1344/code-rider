require 'gosu'
class TextSounds < Component

  def initialize(game_object)
    super
    @game_object = game_object
  end  

  def snd_return
    @@snd_return ||= Gosu::Sample.new(
      $window, Utils.sounds('return.wav'))
  end

  def snd_error
    @@snd_error ||= Gosu::Sample.new(
      $window, Utils.sounds('error.wav'))
  end

  def play_return
    snd_return.play(1, 1, false)
  end

  def play_error
    snd_error.play(0.3, 1, false)
  end

end

  

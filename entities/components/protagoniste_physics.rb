require 'gosu'
class ProtagonistePhysics < Component
  attr_accessor :speed, :x, :y
  attr_reader :u, :v, :direction

  def initialize(game_object, object_pool)
    super(game_object)
    @object_pool = object_pool
    @map = object_pool.map
    @game_object = game_object
    @speed = 500
    @x, @y = object.x, object.y
    @u, @v = xy_to_uv 
    @direction = -1
  end

  def is_moving?
    @old_u != @u && @old_v != @v
  end

  def old_u
    @old_u
  end

  def old_v
    @old_v
  end

  def set_uv(u,v)
    @game_object.insert_move(u,v)
    @old_u, @old_v = @u, @v
    @u, @v = u, v
  end

  def set_xy(x,y)
    @x, @y = x, y
  end

  def uv_to_xy
    [(@u * 128), (@v * 128)]
  end

  def xy_to_uv
    [(@x / 128), (@y / 128)]
  end

  def set_xy_from_uv
    @x, @y = uv_to_xy
  end

  def set_uv_form_xy
    @u, @v = xy_to_uv
  end



  def aligned?
    
    dest_x, dest_y = uv_to_xy
    delta_x = dest_x - @x
    delta_y = dest_y - @y

    #    (delta_x.abs < Utils.tolerance_spot &&
    #      delta_y.abs < Utils.tolerance_spot)
    delta_x.abs < 20 && delta_y.abs < 20 
  end

  def can_move_to?(x, y)
    @map.can_move_to(x, y)
  end

  def transiting?
    @transiting
  end

  def destination( dest_u, dest_v ) # u et v sont les coordonnée de grille

  end      

  def button_down(id)
    if aligned?
      case id
      when Gosu::KbRight 
        set_uv(@u + 1, @v)
        @direction = 0
        puts "intention à droite"
      when Gosu::KbUp 
        set_uv(@u, @v - 1)
        @direction = 1
        puts "intention en haut"
      when Gosu::KbLeft 
        set_uv(@u - 1, @v)
        @direction = 2
        puts "intention à gauche"
      when Gosu::KbDown
        set_uv(@u, @v + 1)
        @direction = 3
        puts "intention en bas"
      end
    end
  end

  def move_to(dir)
    if aligned?
    case dir
      when 0 
        set_uv(@u + 1, @v)
        @direction = 0
        puts "intention à droite"
      when 1 
        set_uv(@u, @v - 1)
        @direction = 1
        puts "intention en haut"
      when 2 
        set_uv(@u - 1, @v)
        @direction = 2
        puts "intention à gauche"
      when 3 
        set_uv(@u, @v + 1)
        @direction = 3
        puts "intention en bas"
      end
    end
  end

  def update
    unless aligned?
      move
    end

    if aligned?
      @direction = -1
    end
  end

  private


  def move
    unless aligned?
      new_x, new_y = @x, @y
      shift = 10 #Utils.adjust_speed(@speed)
      case @direction
      when 0
        new_x += shift
      when 1
        new_y -= shift
      when 2 
        new_x -= shift
      when 3
        new_y += shift
      end
      @x, @y = new_x, new_y
    end

    object.x = @x
    object.y = @y

    if aligned?
      set_xy_from_uv
      object.x = @x
      object.y = @y
    end
  end
end

class BadElectronSounds < Component
  def initialize(game_object)
    @game_object = game_object
  end

  def move_it
   sound.play(1, 1, false)
  end

  private
  def sound
    @@sound ||= Gosu::Sample.new(
      $window, Utils.sounds('electron4.wav'))
  end
end


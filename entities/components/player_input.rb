require 'gosu'
require '../component'

class PlayerInput < Component
  def initialize(camera)
    super(nil)
    @camera = camera
  end

  def control(obj)
    self.object = obj
  end

  def update
    
    if Gosu::KbArrowLeft
      self.direction = 0
    elsif Gosu::KbArrowUp
      self.direction = 1
    elsif Gosu::KbArrowRight 
      self.direction = 2
    elsif Gosu::KbArrowDown
      self.direction = 3
    end
    # logique de mouvement
  end

  
end


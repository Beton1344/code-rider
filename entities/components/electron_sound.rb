require 'gosu'
class ElectronSounds < Component
  def initialize
    super
  end

  def update
  end

  def isnear?
    #if distance is proche de 
    # protagoniste, start menacing sound
  end

  def moving_sound
    @@moving_sound ||= Gosu::Sample.new(
      $window, Utils.sounds('electron_moving.wav'))
  end

  def play
    moving_sound.play
  end
end


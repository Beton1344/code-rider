require 'gosu'

class ContactGraphics < Component
  FRAME_DELAY = 33.66

  def initialize(game_object)
    super
    @game_object = game_object
    @current_frame = 0
  end

  def draw( viewport )
    image = current_frame
    image.draw(
      @game_object.protagoniste.physics.x,
      @game_object.protagoniste.physics.y,
      120)
  end

  def update
    puts "current frame in contact #{@current_frame} "
    now = Gosu.milliseconds
    delta = now - (@last_frame ||= now)
    if delta > FRAME_DELAY
      @last_frame = now 
    end
    @current_frame += (delta / FRAME_DELAY).floor 
    object.mark_for_removal if done?
  end


  def current_frame
    animation[@current_frame % animation.size]
  end

  def done?
    @done ||= @current_frame >= animation.size
  end

  private
  def animation
    @@animation ||=
      Gosu::Image.load_tiles(
        $window, Utils.tiles('contact.png'),
        128, 128, false)
  end
end


require 'gosu'
require_relative 'component'

class LauncherGraphics < Component
  attr_accessor :stopfire
  FRAME_DELAY = 100.66

  def initialize(game_object)
    super(game_object)
    @current_frame = 0
    @stopfire = false 
  end

  def draw(viewport)
    if stopfire
      image = animation[0] 
    else
      image = current_frame
    end
    image.draw(
      x, 
      y, 
      20)
  end

  def update
    now = Gosu.milliseconds
    delta = now - (@last_frame ||= now)
    if delta > FRAME_DELAY
      @last_frame = now
    end
    @current_frame += (delta / FRAME_DELAY).floor
  end

  private
  def current_frame
    animation[@current_frame % animation.size]
  end

  def animation
    @@animation ||=
      Gosu::Image.load_tiles(
        $window, Utils.tiles('mapUnit.png'), 
        128, 128, false)[8..11]
  end
end

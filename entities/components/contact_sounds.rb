class ContactSounds
  class << self
    def play
      sound.play
    end

    private
    def sound
      @@sound ||= Gosu::Sample.new(
        $window, Utils.sounds('electron4.wav'))
    end
  end
end


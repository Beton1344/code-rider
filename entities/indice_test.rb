def get_indice(val)
    (6 * (val - 0.05 / 0.90)).to_i
end


puts "#{get_indice(0.05)} should be 0"
puts "#{get_indice(0.25)} should be 2"
puts "#{get_indice(0.45)} should be 4"
puts "#{get_indice(0.65)} should be 0"
puts "#{get_indice(0.95)} should be 0"




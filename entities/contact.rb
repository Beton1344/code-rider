require 'gosu'
require_relative 'game_object'
require_relative './components/contact_graphics.rb'
require_relative './components/contact_sounds.rb'
class Contact < GameObject
  attr_accessor :x, :y, :u, :v, :protagoniste, :graphics

  def initialize(object_pool, protagoniste)
    super(object_pool)
    @protagoniste = protagoniste
    @graphics = ContactGraphics.new(self)
    ContactSounds.play
#    @protagoniste.energy_down(2)
  end
  def draw(viewport)
    @graphics.draw(viewport)
  end
  def update
    @graphics.update
  end
end



require 'gosu'
require_relative '../entities/game_object'
require_relative '../entities/components/protagoniste_graphics'
require_relative '../entities/components/protagoniste_physics'
require_relative '../entities/components/protagoniste_sounds'
require_relative '../entities/contact'
class Protagoniste < GameObject
  attr_accessor :x, :y, :u, :v, :physics, :sound

  def initialize(object_pool, x, y)
    super(object_pool)
    @x, @y = x, y
    @physics = ProtagonistePhysics.new(self, object_pool)
    @graphics = ProtagonisteGraphics.new(self)
    @piste = []
    @sound = ProtagonisteSounds.new(self)
    @energy = 20 
    @last_time = Gosu.milliseconds
  end

  def button_down(id)
    @physics.button_down(id)
  end

    
  def update
    @physics.update
    @graphics.update
    unless @contact.nil?
      @contact.update 
      @contact = nil if @contact.graphics.done?
    end
  end

  def energy_down(x)
    now = Gosu.milliseconds
    if now - @last_time > 5000
    @contact = Contact.new(object_pool, self)
    @energy -= x 
    @last_time = now
    end 
  end

  def energy_up(x)
    @energy += x
  end

  def energy
    @energy
  end

#  def bumped
#    @physics.set_uv(@physics.u + rand(-2..2), @physics.v + rand(-2..2))
#  end

  def insert_move(u, v)
    @sound.sound_move
    puts "into the insert move with #{u}, #{v}"
    @piste << [u, v]
  end

  def get_next_move
    move = @piste[0]
    @piste = @piste.drop(1)
    puts "into_get_next move is #{move.inspect}"
    move
  end
end

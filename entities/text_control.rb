require 'gosu'
require_relative './../extract'
require_relative './text_field'
require_relative './components/text_sounds.rb'

class TextControl < GameObject
  def initialize( object_pool, camera, protagoniste ) 
    super(object_pool)
     @database = Extractor.new
     @font = Gosu::Font.new($window, Gosu::default_font_name, 20)
     @camera = camera
     @text_fields = Array.new(5)
     @sounds = TextSounds.new(self)
     @protagoniste = protagoniste
     @map = object_pool.map
     @challenges = @database.select_super_easy.reject do |x|
       x.size > 50 
     end.shuffle
     @challenge_effective = 0
     init
  end

  def tower_manager=(tower_manager)
    @tower_manager = tower_manager 
  end
  def init
    puts "in init"
    @database.extractDirectory
    @text_fields[0] = TextField.new(@font, 680, 500)
    @text_fields[1] = TextField.new(@font, 350, 20)
    @text_fields[2] = TextField.new(@font, 20, 300)
    @text_fields[3] = TextField.new(@font, 350, 730)
    @text_fields[4] = TextField.new(@font, 350, 650)
    update_texts_fields
    @text_fields[4].update_text("")
    $window.text_input = nil #@text_fields[4]


    puts "#{$window.text_input}"
  end

  def change_text(num, content)
    @text_fields[num].update_text(content)
  end

  def update
    @text_fields.each { |tf| tf.update_position(@camera.viewport) }
  end


  def draw(viewport)
    if $window.text_input == nil
      (0..3).each do |i|
        @text_fields[i].draw
      end
    else
      @text_fields.each { |tf| tf.draw }
    end
  end

  def update_texts_fields
    (0..3).each do |i|
      @text_fields[i].update_text(@challenges[@challenge_effective + i])
    end
    @challenge_effective += 4 
  end

  def button_down(id)
    case id
    when Gosu::KbEscape
      puts "inside escape"
      if $window.text_input.nil?
        $window.text_input = @text_fields[4]
      else
        $window.text_input = nil
      end

    when Gosu::Kb1
      @chalenges = @database.select_super_easy.shuffle if $window.text_input.nil?
    when Gosu::Kb2 
      @chalenges = @database.select_easy.shuffle if $window.text_input.nil?
   when Gosu::Kb3
      @chalenges = @database.select_medium.shuffle if $window.text_input.nil?
    when Gosu::Kb4
      @chalenges = @database.select_difficulty.shuffle if $window.text_input.nil?
    when Gosu::KbReturn
      found = false
      @sounds.play_return
      text_comp = @text_fields[4].get_text
      text_comp = text_comp.chomp.strip
      (0..3).map do |i|
        if is_the_same( @text_fields[i].get_text, text_comp)
          @protagoniste.physics.move_to(i)
          found = true
        end
      end
      unless found
        @sounds.play_error
        @tower_manager.attack
      end
      @text_fields[4].update_text("")
      update_texts_fields
      
    end

  end

  def is_the_same( x, y )
    
    x.delete(' ') == y.delete(' ')
  #  x.squeeze == y.squeeze
  end
  def target=(target)
    @target = target
    @x, @y = target.x, target.y
  end


end

require 'gosu'
require_relative './tower'

class TowerManager < GameObject

  attr_accessor :towers, :towers_coord
  def initialize(object_pool, protagoniste)
    super(object_pool)
    @protagoniste = protagoniste
    @map = object_pool.map
    @towers = []
    @towers_coord = []
    set_towers
  end
  
  def set_towers
    @map.towers.each do |uv|
      puts "puting tower"
      tower = Tower.new(object_pool, uv[0], uv[1])
      tower.target = @protagoniste
      @towers << tower 
      @towers_coord << uv
      puts @towers.compact.size
    end
  end

  def attack
    #puts @towers.inspect
    result = @towers.select { |x| x.is_near? }.shuffle
    (0..1).each { |x| result[x].fire }
  end

end




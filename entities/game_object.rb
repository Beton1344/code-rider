class GameObject
  attr_accessor :x, :y, :u, :v
  def initialize(object_pool)
    @components = []
    @object_pool = object_pool
    @object_pool.objects << self
    @speed = 5
  end

  def components
    @components
  end

  def update
    @components.map(&:update)
  end

  def draw(window)
    @components.each { |c| c.draw(window) }
  end

  def removable?
    @removable
  end

  def mark_for_removal
    @removable = true
  end

  
  protected
  def object_pool
    @object_pool
  end
end


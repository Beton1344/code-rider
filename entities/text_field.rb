require 'gosu'

class TextField < Gosu::TextInput
  
  INACTIVE_COLOR = 0xdd000000
  ACTIVE_COLOR = 0xCCFF4444
  SELECTION_COLOR = 0xAADD1111
  PADDING = 5

  BEST_WIDTH = 500
  attr_reader :x, :y

  def initialize( font, x, y)
    super()
  
    @font, @x, @y = font, x, y
    @original_x, @original_y = @x, @y

    self.text = "lorem ipsum bla bla"
  end

  def update_text( content )
    self.text = content
  end

  def get_text
    self.text
  end

  def update_position(viewport)
    if $window.text_input == self
      @x = (@original_x / Utils.camera_zoom) +\
            (viewport[0] + viewport[1])/2 
      @y = (@original_y / Utils.camera_zoom) +\
        (viewport[2] + viewport[3])/2 + (Utils.tile_size)
    end 
      @x = (@original_x / Utils.camera_zoom) + viewport[0] 
      @y = (@original_y / Utils.camera_zoom) + viewport[2] 
  end

  def draw
    if $window.text_input == self
      background_color = ACTIVE_COLOR
    else
      background_color = INACTIVE_COLOR
    end

    $window.draw_quad(x - PADDING,           y - height + PADDING, background_color,
                      x + width + PADDING,    y - height + PADDING, background_color, 
                      x - PADDING, y + height + PADDING, background_color,
                      x + width + PADDING, y + height+ PADDING, background_color, 80)

    # Caculer la position du caret
    pos_x = x + @font.text_width(self.text[0...self.caret_pos])
#    sel_x = x + @font.text_width(self.text[0...self.selection_start])

    if $window.text_input == self
      $window.draw_quad( pos_x, y, SELECTION_COLOR,
                        pos_x + 10, y, SELECTION_COLOR, 
                        pos_x, y + height, SELECTION_COLOR, 
                        pos_x + 10, y + height, SELECTION_COLOR, 110)
    end
    @font.draw(self.text, x, y, 85)
  end

  def width
    [@font.text_width(self.text), BEST_WIDTH].max
  end

  def height
    @font.height
  end


end


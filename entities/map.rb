require 'perlin_noise'
require 'gosu'
#require_relative '../main.rb'
class Map
  MAP_WIDTH = 100 #Utils.MAP_WIDTH
  MAP_HEIGHT = 100 #Utils.MAP_WIDTH
  TILE_SIZE = 128 #Utils.TILE_SIZE

  attr_accessor :types, :chips_values, :chips_setted, :towers

  def initialize
    load_tiles
    package = generate_map
    @map = package[0]
    @types = package[1]
    @object = []
    @chips_values = []
    @chips_setted = []
    @towers = []
    set_chips
  end

  def find_spawn_point
    x = MAP_WIDTH / 2 * TILE_SIZE
    y = MAP_HEIGHT / 2 * TILE_SIZE
    [x, y]
  end 

  def draw(viewport)
    viewport.map! { |p| p / TILE_SIZE }
    x0, x1, y0, y1 = viewport.map(&:to_i)
    (x0..x1).each do |x|
      (y0..y1).each do |y|
        row = @map[x]
        if row
          tile = @map[x][y]
          map_x = x * TILE_SIZE
          map_y = y * TILE_SIZE
          @map[x][y].draw(map_x, map_y, 0) if tile
        end
      end
    end
  end

  private

  def tile_at(x, y)
    t_x = ((x / TILE_SIZE) % TILE_SIZE).floor
    t_y = ((y / TILE_SIZE) % TILE_SIZE).floor
    row = @map[t_x]
    row[t_y] if row
  end

  def type_at(u, v)
    row = @types[u]
    row[v] if row
  end

  def load_tiles
    tiles = Gosu::Image.load_tiles(
      $window,  Utils.tiles('mapUnit.png'), 
      128, 128, true)
    @chipset = tiles[0]
    @board = tiles[1..6]
    @pitcherHold = tiles[7]
    @pitcher = tiles[7..10]
  end

  def generate_map
    map = {}
    types = {}
    (0..MAP_WIDTH).each do |x|
      map[x] = {}
      types[x] = {}
      (0..MAP_HEIGHT).each do |y|
        type = rand(100).to_f/100
        map[x][y] = choose_tile(type)
        types[x][y] = choose_type(type)
      end
    end
    [map,types]
  end

  def set_chips
    (0..MAP_WIDTH).each do |u|
      row = types[u]
      (0..MAP_HEIGHT).each do |v|
        if row[v] == 0
          @chips_values << [u,v]
        end
        if row[v] == 7
          @towers << [u, v]
        end
      end
    end
  end

  def choose_type(val)
    case val
    when 0.0..0.02
      0
    when 0.02..0.97
      1 + get_indice(val)
    when 0.97..1.00
      7
    end
  end
  def choose_tile(val)
    case val
    when 0.0..0.02 # 1/20 chance
      @chipset
    when 0.02..0.97
      @board[get_indice(val)]
    when 0.97..1.0
      @pitcherHold
    else
      @board[2]
    end
  end

  def get_indice(val)
    (6 * (val - 0.02 / 0.97)).to_i
  end
end


      

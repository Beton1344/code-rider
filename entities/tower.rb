require 'gosu' 
require_relative './launcher'

class Tower < GameObject
  attr_accessor :x, :y, :target_u, :target_v

  def initialize(object_pool, u, v)
    super(object_pool)
    @x, @y = Utils.coordinate_to_xy(u,v)
    @u, @v = u, v
    @launcher = Launcher.new(object_pool, @x, @y)
    @past = Gosu.milliseconds
    @last_missile = Gosu.milliseconds
    @can_fire = true
  end

  def target=(target)
    @target=target
  end

  def fire
    if @can_fire
      @launcher.stopfire = false
      @last_missile = Gosu.milliseconds
      Electron.new(object_pool, @x, @y, @target)
      @can_fire = false
    end
  end
  
 
  def update
    now = Gosu.milliseconds
    if 5000 < now - @last_missile
      @can_fire = true 
    end
  end

  def is_near?
    dist_u = @target.physics.u - @u
    dist_v = @target.physics.v - @v
    result = dist_u.abs + dist_v.abs
    if result > 2 && result < 20
      return true
    end
    false
  end
  
end

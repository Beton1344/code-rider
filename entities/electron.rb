require_relative './components/electron_graphics'
require_relative './components/electron_physics'
require_relative './components/electron_sound'
require_relative './contact'
class Electron < GameObject
  attr_accessor :x, :y, :target_x, :target_y, :speed, :fired_at

  def initialize(object_pool, source_x, source_y, protagoniste)
    super(object_pool)
    @object_pool = object_pool
    @speed = Utils.electron_speed / 10 
    @protagoniste = protagoniste
    @x, @y = source_x, source_y
    @target_u, @target_v = @protagoniste.physics.u, @protagoniste.physics.v
    @target_x, @target_y = Utils.coordinate_to_xy(@target_u, @target_v)
    @fired_at = Gosu.milliseconds
    ElectronGraphics.new(self)
    ElectronPhysics.new(self)
    @sound = ElectronSounds.new
  end

  def explode
   # Contact.new(@object_pool, @protagoniste)  
    @protagoniste.energy_down(2)
    mark_for_removal
  end

  def fire(speed)
    @sound.play(0.5, 1)
    @speed = speed
    @fired_at = Gosu.milliseconds
  end

end

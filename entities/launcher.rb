require_relative './game_object.rb'
require_relative './components/launcher_graphics'
class Launcher < GameObject
  def initialize(object_pool, x, y)
    super(object_pool)
    @current_frame = 0
    @x, @y = x, y
    @launcher = LauncherGraphics.new(self) 
  end


  def stopfire=(value)
    @launcher.stopfire = true
  end
end

  

require 'gosu'
require_relative '../entities/game_object'
require_relative '../entities/components/chip_graphics'

class Chip < GameObject
  attr_accessor :x, :y, :u, :v

  def initialize(object_pool, u, v)
    super(object_pool)
    puts "kallisssseeeee"
    @u, @v = u, v
    @x, @y = Utils.coordinate_to_xy(u, v) 
    @graphics = ChipGraphics.new(self)
  end

  def update
    @graphics.update
  end
end


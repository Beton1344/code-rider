Ce dossier contient l'ensemble des ressources pour la création d'un jeu
fait dans le cadre du cour inf5071 donné les mercredi après-midi,
par Alexandre Blondin-Massé, à l'automne 2016 et ceci,
à l'Université du Québec à Montréal. 

Titre : Code Rider
Tagline : Pour se taper des heures de plaisir
Date de dépot : 8 nov 2016
Version : 0.1.1
Titre de version : Preuve de concept
Auteur : Eric Lavallée

Ce git est en place pour fins d'études. Aussi, ceci constitue un projet 
personnel à long terme qui sera bonifié au fil du temps. 

Auteur : Eric Lavallée

Screenshots dans supplements/<xxx>_screenshot.png !!!!!!!!!!!
==========================================================================
Code rider ========================== Le jeu

Bref -----
Code rider est un jeu de frappe de clavier qui vous donne les commandes
d'un bout de code qui tente de faire son chemin sur une motherboard afin 
de placer des processeurs sur les chipsets ouverts disponibles. 
Pour se diriger, le joueur doit taper les lignes de code qui sont disposées
en haut, à droite, à gauche et en bas. Si le joueur veut aller en bas, il 
n'a qu'à taper la ligne du bas. Il en va de même pour toutes les directions. 

Contrôle (debug tryout) ----
<esc> toggle mode (demonstration, jeu actif)
demonstration
pour bouger le protagoniste
<ArrowUp>, <ArrowDown>, <ArrowRight>, <ArrowLeft>

jeu actif
pour bouger le protagoniste
taper la ligne de code associé à la direction voulue. 
La ligne de frappe est comparée en appuyant sur <enter>
Si la frappe correspond au texte, le protagoniste bougera
dans la direction voulue. 
::: note : les espaces sont éléminés avant l'évaluation d'équivalent, cependant
            la CASE EST RESPECTÉE. 

==========================================================================
Statut =================================================================== 

Le code est "quick and dirty", j'espère que les noms des fonctions et variables
sont assez self-explanatory. Beaucoups de raccourcis ont été pris pour cette 
preuve de concept. Ceci étant dit, la prochaine étape est celle de refactoriser 
tout en documentant le code.

Cette première itération est une preuve de concept qui met en application les 
fonctionnalité suivantes : 
  - Extraction de code sources et classement des lignes extraites
    par niveau de difficulté. 
  - Validation des lignes en faisant abstraction des espaces
  - Génération de niveau aléatoire
  - Déplacement du héro
  - Déplacement de l'enemis qui suit le chemin du héro (badelectron)
  - Installation des processeurs sur le board
  - Points de vie et compte des processeurs installés
  - Dommages inculqués
  - Musique et effets sonores

==========================================================================
Objectifs futurs   =======================================================

prochains ----
La prochaine itération veut :
  - améliorer le rytme du badelectron pour suivre celui du protagoniste
  - augmenter l'offre de languages
  - offrir l'option d'utiliser un language de son choix
  - ajouter la coloration syntaxique
  - avoir un mouvement lié à la performance de frappe de l'utilisateur
  - ajouter des tuiles de jeu avec des éléments conséquents ( tuile avec
              effets genre powerup) 
  - ajouter d'autre ennemis et des moyens de les contrer
  - ajouter une minimap 
  - ajouter une barre de statut (combien de processeurs d'installée, vie, 
              performance de frappe, etc. )
  - avoir un tutoriel
  - générer la navigation d'un écran à l'autre (menu, partie)
  - mode multiplayer cooperatif
  - mode multiplayer cyberwarzone ( defense de territoire, capture the flag )
  - mode de caméra dynamique ( zoom en relation des éléments de proximité, 
              ennemis, etc. )
  - ... plein d'autres affaires tripantes.

Aussi, développer des contact avec des artistes, notamments du coté
de la musique et des effets sonores. 

==========================================================================
Licences =================================================================
Pour l'ensemble du code ruby
Auteur : Eric Lavallée
Vous pouvez critiquer, observer, vous amuser à modifier, améliorer le code à
votre guise et péril. Si vous avez des suggestions, implémentez-les, soumettez
les branches.  

Pour le code source utilisé pour le contrôle du protagoniste 
ICI : Code source posix du module string en C. 

Le concept général de code rider et de sa signature graphique
Auteur : Eric Lavallée 
Le concept général de "CODE RIDER" est la propriété de 
l'auteur et ce dernier, au stade de ce développement du jeu 
est assujetie au droit d'auteur du Canada et des traités internationnaux 
qui s'appliquent.

Pour les assets graphiques
Auteur : Eric Lavallée 
L'auteur donne une license d'utilisation non-commerciale. 

Pour les assets sonores et musicaux
Les musiques sont tirées de Wolfram Tones qui confère une utilisation
non-commerciale des produits dérivé de leurs systèmes. (voir plus bas pr ref)
Les sons sont tiré de Pro-Score, une banque de son et utilisé sous licences. 
En ce sens, elle font partie du projet et sont distribué en tant qu'éléments 
de code rider. L'auteur de code rider ne concèdent aucune licence relatifs
à ces éléments, leurs droits sont assujetties aux conditions émise par 
les auteurs respectifs. De manière prudente, n'utilisez pas ces ressources
en dehors de ce projet. 

==========================================================================
Organisations des dossiers ===============================================

Les dossiers sont les suivant : 
  fonts/ : contient des fonts utilisés potentiellement dans le jeu
  music/ : contient les musiques du menu d'acceuil, du jeu et du menu "game over"
  sounds/ : contient les effets sonores utilisé dans le jeu (déplacement,
    contact, etc...)
  source_code : contient les fichiers sources utilisé pour les textes 
    utilisé pour le contrôle du personnage. 
  sprites/ : contient l'ensemble des sprite sheet nécéssaire au jeu. 
  states/ : contient les fichier d'état (menu, jeu, game_over...)
  entities/ : contient les entités du jeu
  entities/component : contient les composants des entités du jeu. 

Pour voir les gifs des assets ============================================
  supplement/gifs/

Pour la présentation (basique) beamer (powerpoint) =======================
  supplement/coderiderBeamer.pdf

==========================================================================
Execution/instalation ====================================================
Ce jeu est écrit en ruby et le gem gosu est nécéssaire pour fonctionner. 

Voici toutes les dépendances installées sur ma machine, avec ceci,
Code rider devrait fonctionner correctement. 

--- je sais que plusieurs ne sont pas relié directement mais, ca marche ici,
avec ca. 

Version de ruby
- ruby-2.2.5
Gems installés (C'est ce que j'ai installé pour le faire rouler)
- activemodel (4.2.7.1)
- activerecord (4.2.7.1)
- activesupport (4.2.7.1)
- arel (6.0.3)
- ashton (0.1.6)
- bigdecimal (1.2.6)
- builder (3.2.2)
- bundler-unload (1.0.2)
- chimpmunk (0.0.9)
- chingu (0.8.1)
- climate_control (0.0.3)
- cocaine (0.5.8)
- conject (0.1.8)
- crack (0.4.3)
- cri (2.1.0)
- domain_name (0.5.20160826)
- executable-hooks (1.3.2)
- faraday (0.9.2)
- faster_xml_simple (0.5.0)
- ffi (1.9.14)
- gamebox (0.5.5)
- gem-wrappers (1.2.7)
- gibbon (2.2.4)
- gosu (0.10.8)
- http-cookie (1.0.3)
- httpclient (2.8.2.4)
- i18n (0.7.0)
- io-console (0.4.3)
- json (1.8.1)
- kvo (0.1.0)
- libxml-ruby (2.9.0)
- listen (3.1.5)
- mime-types (3.1)
- mime-types-data (3.2016.0521)
- mimemagic (0.3.2)
- mini_portile2 (2.1.0)
- minitest (5.9.1)
- multi_json (1.12.1)
- multipart-post (2.0.0)
- net-github-upload (0.0.8)
- netrc (0.11.0)
- nokogiri (1.6.8.1)
- ocra (1.3.6)
- opengl (0.9.2)
- paperclip (5.1.0)
- psych (2.0.8)
- publisher (1.1.2)
- rake (10.4.2, 0.9.2.2)
- rb-fsevent (0.9.7)
- rb-inotify (0.9.7)
- rdoc (4.2.0)
- releasy (0.2.2)
- require_all (1.3.3)
- rest-client (2.0.0)
- rmagick (2.16.0)
- ruby_dep (1.5.0)
- rubygems-bundler (1.4.4)
- rvm (1.11.3.9)
- safe_yaml (1.0.4)
- thor (0.14.6)
- thread_safe (0.3.5)
- tween (0.1.2)
- tzinfo (1.2.2)
- unf (0.1.4)
- unf_ext (0.0.7.2)

La musique provient de l'application Wolphram Sound. C'est de la
musique généré par des fonctions mathématiques. 
	- http://tones.wolfram.com/generate/
Ce générateur permet l'usage personnel et non-commercial des musiques créées. 
	
Tous les assets visuels sont le produit de mon imagination et ont
été créés avec l'un ou l'ensemble des produit graphiques suivants: 
	- Blender
	- Photoshop
	- After effect
	- Illustrator

La structure du code et l'organisation des classes est fortement lié
avec le livre-tutoriel de Gosu. Cependant, cette structure est simplement
une bonne pratique vue dans d'autre cadre de production de jeu. 
	- Developing Games With Ruby
		Tomas Varaneckas

Les effets sonores sont pris d'une banque de son dont j'ai acheté les 
droits. Certains de ses sons ont été modifiés à l'aide de SoundBooth, 
produit d'édition sonore publié par Adobe. 


